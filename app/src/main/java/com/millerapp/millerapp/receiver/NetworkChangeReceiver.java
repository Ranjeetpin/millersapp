package com.millerapp.millerapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.millerapp.millerapp.Util.ConnectionDetector;

/**
 * Created by Ranjeet on 7/14/2016.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
//    ConnectionDetector cd;
    ConnectionDetector cd;

    @Override
    public void onReceive(Context context, Intent intent) {

        cd =new ConnectionDetector(context);

boolean status= cd.isConnectingToInternet();

        Log.d("Ranjeet", "OTP received: " + status);

        Bundle extras = intent.getExtras();
        Intent i = new Intent("broadCastName");
        // Data you need to pass to activity
        i.putExtra("message", status);

        context.sendBroadcast(i);

    }
}
