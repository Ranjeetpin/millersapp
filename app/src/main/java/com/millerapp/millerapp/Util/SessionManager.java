package com.millerapp.millerapp.Util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Ranjeet on 8/23/2016.
 */
public class SessionManager {

    SharedPreferences sharedprefernce;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE=0;



    private static final String PREF_NAME="sharedMillerApp";
    private static final String CURKMSYEAR="curkmsyear";
    private static final String CURKMSYEARFULL="kmsyearfull";
    private static final String CURKMSYEAR1="kmsyear";
    private static final String CURKMSYEAR2="kmsseason";
    private static final String USERCODE="userCode";
    private static final String DESIGNATION="designation";
    private static final String IS_LOGIN="IsUserLogIn";

    public  SessionManager(Context context){


        this.context =  context;
        sharedprefernce = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor=sharedprefernce.edit();

    }

    public void setCurkmsyear(String curkmsyear){

    editor.putString(CURKMSYEAR,curkmsyear);

    editor.commit();
    }

    public void setLogin(){

        editor.putBoolean(IS_LOGIN, true);
        editor.commit();

    }
    public Boolean isLogin(){
        return sharedprefernce.getBoolean(IS_LOGIN, false);

    }
    public String getCurkmsyear(){


        return sharedprefernce.getString(CURKMSYEAR,"DEAFULT");
    }




    public void setCurkmsyearfull(String curkmsyearfull){

        editor.putString(CURKMSYEARFULL,curkmsyearfull);

        editor.commit();
    }


    public String getCurkmsyearfull(){


        return sharedprefernce.getString(CURKMSYEARFULL,"DEAFULT");
    }




    public void setCurkmsyear1(String kmsyear){

        editor.putString(CURKMSYEAR1,kmsyear);

        editor.commit();
    }

    public String getCurkmsyear1(){


        return sharedprefernce.getString(CURKMSYEAR1,"DEAFULT");
    }





    public void setCurkmsyear2(String kmsseason){

        editor.putString(CURKMSYEAR2,kmsseason);

        editor.commit();
    }

    public String getCurkmsyear2(){


        return sharedprefernce.getString(CURKMSYEAR2,"DEAFULT");
    }




    public void setUsercode(String usercode){

        editor.putString(USERCODE,usercode);

        editor.commit();
    }

    public String getUsercode(){


        return sharedprefernce.getString(USERCODE,"DEAFULT");
    }





    public void setDesignation(String designation){

        editor.putString(DESIGNATION,designation);

        editor.commit();
    }

    public String getDesignation(){


        return sharedprefernce.getString(DESIGNATION,"DEAFULT");
    }


}
