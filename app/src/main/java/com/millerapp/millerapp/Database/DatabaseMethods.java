package com.millerapp.millerapp.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.millerapp.millerapp.Constant;
import com.millerapp.millerapp.Models.Information;
import com.millerapp.millerapp.Models.UserModel;
import com.millerapp.millerapp.Util.SessionManager;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Manab Boro on 1/4/2016.
 */
public class DatabaseMethods {
    Context context;
    SQLiteDatabase db;
    DBHelper helper;
    String currentTime;
    SessionManager sessionManager;
    public DatabaseMethods(Context context) {
        this.context = context;
        helper = new DBHelper(context);
        db = helper.getWritableDatabase();
        sessionManager=new SessionManager(context);
    }

    public List<Information> getAllData() {
        List<Information> list = new ArrayList<>();

        String selection = DBHelper.USERNAME+ " = '" + sessionManager.getUsercode() + "'";

//                + " AND " + DBHelper.mREC_DATETIME + " = '" + TPDateFrom + "'"
//                + " AND " + DBHelper.mREC_DATETIME + " = '" + TPDateTo + "'";

        SQLiteDatabase database = helper.getWritableDatabase();
        String[] column = {DBHelper.UID, DBHelper.mTRP, DBHelper.mPACS_CODE, DBHelper.mMY_CODE, DBHelper.mYEAR, DBHelper.mSEASON, DBHelper.mTR_PASS_NO, DBHelper.mCM_CODE, DBHelper.mBAG, DBHelper.mQTY, DBHelper.V_REG_NO, DBHelper.mREC_DATETIME, DBHelper.mIS_DELETED, DBHelper.mYEAR, DBHelper.status, DBHelper.QRCode};
        Cursor cursor = database.query(DBHelper.DATABASE_TABLE_NAME, column, selection, null, null, null, null);


        while (cursor.moveToNext()) {
            Information data = new Information();
            data.UID = cursor.getLong(cursor.getColumnIndex(DBHelper.UID));
            data.mTRP = cursor.getString(cursor.getColumnIndex(DBHelper.mTRP));
            data.mPACS_CODE = cursor.getString(cursor.getColumnIndex(DBHelper.mPACS_CODE));
            data.mMY_CODE = cursor.getString(cursor.getColumnIndex(DBHelper.mMY_CODE));
            data.mYEAR = cursor.getString(cursor.getColumnIndex(DBHelper.mYEAR));
            data.mSEASON = cursor.getString(cursor.getColumnIndex(DBHelper.mSEASON));
            data.mTR_PASS_NO = cursor.getString(cursor.getColumnIndex(DBHelper.mTR_PASS_NO));
            data.mCM_CODE = cursor.getString(cursor.getColumnIndex(DBHelper.mCM_CODE));
            data.mBAG = cursor.getString(cursor.getColumnIndex(DBHelper.mBAG));
            data.mQTY = cursor.getString(cursor.getColumnIndex(DBHelper.mQTY));
            data.V_REG_NO = cursor.getString(cursor.getColumnIndex(DBHelper.V_REG_NO));
            data.mREC_DATETIME = cursor.getString(cursor.getColumnIndex(DBHelper.mREC_DATETIME));
            data.mIS_DELETED = cursor.getString(cursor.getColumnIndex(DBHelper.mIS_DELETED));
            data.status = cursor.getString(cursor.getColumnIndex(DBHelper.status));
            data.QRCode = cursor.getString(cursor.getColumnIndex(DBHelper.QRCode));


            //  Log.d("Restored id ", listModel.id + "");

            list.add(data);

        }
        database.close();
        return list;
    }

    public List<Information> isUserAvailable(String username) {
        List<Information> list = new ArrayList<>();

        String selection = DBHelper.USERNAME + " = '" + username + "'";


        SQLiteDatabase database = helper.getWritableDatabase();
        String[] column = {DBHelper.UID, DBHelper.USERNAME};
        Cursor cursor = database.query(DBHelper.DATABASE_TABLE_NAME, column, selection, null, null, null, null);


        while (cursor.moveToNext()) {
            Information data = new Information();
            data.UID = cursor.getLong(cursor.getColumnIndex(DBHelper.UID));
            list.add(data);

        }
        database.close();
        return list;

    }

    public List<Information> getSearchedData(String society, String ppc, String TPDateFrom, String TPDateTo) {
        List<Information> list = new ArrayList<>();
        SessionManager sessionManager=new SessionManager(context);

        String selection = DBHelper.mMY_CODE + " = '" + society + "'"
                + " AND " + DBHelper.mPACS_CODE + " = '" + ppc + "'"
                + " AND " + DBHelper.USERNAME + " = '" + sessionManager.getUsercode() + "'";
//                + " AND " + DBHelper.mREC_DATETIME + " = '" + TPDateFrom + "'"
//                + " AND " + DBHelper.mREC_DATETIME + " = '" + TPDateTo + "'";

        SQLiteDatabase database = helper.getWritableDatabase();
        String[] column = {DBHelper.UID, DBHelper.mTRP, DBHelper.mPACS_CODE, DBHelper.mMY_CODE, DBHelper.mYEAR, DBHelper.mSEASON, DBHelper.mTR_PASS_NO, DBHelper.mCM_CODE, DBHelper.mBAG, DBHelper.mQTY, DBHelper.V_REG_NO, DBHelper.mREC_DATETIME, DBHelper.mIS_DELETED, DBHelper.mYEAR, DBHelper.status, DBHelper.QRCode};
        Cursor cursor = database.query(DBHelper.DATABASE_TABLE_NAME, column, selection, null, null, null, null);


        while (cursor.moveToNext()) {
            Information data = new Information();
            data.UID = cursor.getLong(cursor.getColumnIndex(DBHelper.UID));
            data.mTRP = cursor.getString(cursor.getColumnIndex(DBHelper.mTRP));
            data.mPACS_CODE = cursor.getString(cursor.getColumnIndex(DBHelper.mPACS_CODE));
            data.mMY_CODE = cursor.getString(cursor.getColumnIndex(DBHelper.mMY_CODE));
            data.mYEAR = cursor.getString(cursor.getColumnIndex(DBHelper.mYEAR));
            data.mSEASON = cursor.getString(cursor.getColumnIndex(DBHelper.mSEASON));
            data.mTR_PASS_NO = cursor.getString(cursor.getColumnIndex(DBHelper.mTR_PASS_NO));
            data.mCM_CODE = cursor.getString(cursor.getColumnIndex(DBHelper.mCM_CODE));
            data.mBAG = cursor.getString(cursor.getColumnIndex(DBHelper.mBAG));
            data.mQTY = cursor.getString(cursor.getColumnIndex(DBHelper.mQTY));
            data.V_REG_NO = cursor.getString(cursor.getColumnIndex(DBHelper.V_REG_NO));
            data.mREC_DATETIME = cursor.getString(cursor.getColumnIndex(DBHelper.mREC_DATETIME));
            data.mIS_DELETED = cursor.getString(cursor.getColumnIndex(DBHelper.mIS_DELETED));
            data.status = cursor.getString(cursor.getColumnIndex(DBHelper.status));
            data.QRCode = cursor.getString(cursor.getColumnIndex(DBHelper.QRCode));


            //  Log.d("Restored id ", listModel.id + "");

            list.add(data);

        }
        database.close();
        return list;

    }


    public long insertItem(String mPACS_CODE, String mMY_CODE, String mYEAR, String mSEASON, String mTR_PASS_NO, String mCM_CODE, String mBAG, String mQTY, String V_REG_NO, String mREC_DATETIME, String mIS_DELETED, String status, String QRCode, String username) {
        SQLiteDatabase database = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.mTRP, getDateTime());
        contentValues.put(DBHelper.mPACS_CODE, mPACS_CODE);
        contentValues.put(DBHelper.mMY_CODE, mMY_CODE);
        contentValues.put(DBHelper.mYEAR, mYEAR);
        contentValues.put(DBHelper.mSEASON, mSEASON);
        contentValues.put(DBHelper.mTR_PASS_NO, mTR_PASS_NO);
        contentValues.put(DBHelper.mCM_CODE, mCM_CODE);
        contentValues.put(DBHelper.mBAG, mBAG);
        contentValues.put(DBHelper.mQTY, mQTY);
        contentValues.put(DBHelper.V_REG_NO, V_REG_NO);
        contentValues.put(DBHelper.mREC_DATETIME, mREC_DATETIME);
        contentValues.put(DBHelper.mIS_DELETED, mIS_DELETED);
        contentValues.put(DBHelper.status, status);
        contentValues.put(DBHelper.QRCode, QRCode);
        contentValues.put(DBHelper.USERNAME, username);


        long id = database.insert(DBHelper.DATABASE_TABLE_NAME, null, contentValues);
        database.close();

        checkInsertResult(id);
        return id;
    }

    private void checkInsertResult(long id) {
        if (id != -1) {
            Toast.makeText(context, "Data inserted successfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Error occured while inserting data", Toast.LENGTH_SHORT).show();
        }
    }

    public void delete(long note_id) {
        db.delete(DBHelper.DATABASE_TABLE_NAME, DBHelper.UID + " = " + note_id, null);
        Log.d("Deleted from id ", note_id + "");
        db.close();
    }

    public int updateData(long id, String status) {
        SQLiteDatabase database = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.status, status);

        database.update(DBHelper.DATABASE_TABLE_NAME, contentValues, DBHelper.UID + "=" + id, null);
        database.close();
        return 1;
    }


    private String getDateTime() {
        Date curDate = new Date();
        return DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
                DateFormat.SHORT).format(curDate);
    }


}