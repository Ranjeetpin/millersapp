package com.millerapp.millerapp.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DBHelper extends SQLiteOpenHelper {
    static final int DATABASE_VERSION = 2;
    static final String DATABASE_NAME = "millar_Database";


    //Millar Constants
    static final String DATABASE_TABLE_NAME = "millar_table";
    static final String UID = "_id";
    static final String mTRP = "mtrp";
    static final String DATE = "date";
    static final String mPACS_CODE = "mpacs";
    static final String mMY_CODE = "mmy_code";
    static final String mYEAR = "myear";
    static final String mSEASON = "mseason";
    static final String mTR_PASS_NO = "pass_no";
    static final String mCM_CODE = "mcn_code";
    static final String mBAG = "mbag";
    static final String mQTY = "mqty";
    static final String V_REG_NO = "mreg_no";
    static final String mREC_DATETIME = "mrec_datetime";
    static final String mIS_DELETED = "mis_deleted";
    static final String status = "status";
    static final String QRCode = "qr_code";
    static final String USERNAME = "usernaem";




    //NOTES SQL
    private static final String CREATE_TABLE_CONTENT = "CREATE TABLE " + DATABASE_TABLE_NAME + "(" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + mTRP + " VARCHAR(225), " + DATE + " VARCHAR(50), " + mPACS_CODE + " VARCHAR(225), " + mMY_CODE + " VARCHAR(2), " + mYEAR + " VARCHAR(10), " + mSEASON + " VARCHAR(10), " + mTR_PASS_NO + " VARCHAR(10), " + mCM_CODE + " VARCHAR(20), " + mBAG + " VARCHAR(20), " + mQTY + " VARCHAR(20), " + V_REG_NO + " VARCHAR(20), " + mREC_DATETIME + " VARCHAR(20), " + mIS_DELETED + " VARCHAR(20), " + status + " VARCHAR(20), " + QRCode + " VARCHAR(20),"+USERNAME+" VARCHAR(20));";
    private static final String DROP_TABLE_NOTE = "DROP TABLE IF EXISTS " + DATABASE_TABLE_NAME;


    Context context;

    public DBHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;


    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CONTENT);

        Toast.makeText(context, "Created Table", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_NOTE);

        Toast.makeText(context, "DropTable", Toast.LENGTH_SHORT).show();

        onCreate(db);
        Toast.makeText(context, "ReCreated Table", Toast.LENGTH_SHORT).show();


    }
}