package com.millerapp.millerapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.ksoap2.serialization.SoapObject;


import com.millerapp.millerapp.Util.SessionManager;
import com.millerapp.millerapp.Webservice.WebService;
import com.millerapp.millerapp.mainClass.LandingPage;

import java.util.ArrayList;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener {

    EditText edtUserName, edtPassword;
    Button btnLogin;
    ProgressDialog pd_login;
    Boolean isFirstTime;
    SessionManager sessionManager;

    Boolean hierarchy = false;
    Boolean showalert = true;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_login_screen);
        getSupportActionBar().hide();

        IntentFilter filter = new IntentFilter("broadCastName");


        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle results = getResultExtras(true);
                hierarchy = intent.getExtras().getBoolean("message");
//
                if (hierarchy) {

//                    submissionData();
//                    Toast.makeText(HistoryDetails.this, "Internet Connect ", Toast.LENGTH_LONG).show();
                    Log.d("Ranjeet", "Intenet back");
                    try {
                        alertDialog.dismiss();
//
                        showalert = true;
                    } catch (Exception e) {
                        Log.d("Ranjeet", hierarchy + "RAnjeet receive valuae of receiver");
                    }


                } else {
                    if (showalert) {
                        showAlertDialog(LoginScreen.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                        showalert = false;

                        Log.d("Ranjeet", hierarchy + "RAnjeet receive valuae of receiver");
                    }
                }


                Log.d("Ranjeet", hierarchy + "RAnjeet receive valuae of receiver");
            }
        }, filter);


        sessionManager = new SessionManager(getApplicationContext());

        if (sessionManager.isLogin()) {
            Intent intentLogin = new Intent(getApplicationContext(), LandingPage.class);
            startActivity(intentLogin);
            finish();

        }

        edtUserName = (EditText) findViewById(R.id.input_email);
        edtPassword = (EditText) findViewById(R.id.input_password);
        btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(this);

        final SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(LoginScreen.this);

        final SharedPreferences.Editor editor = app_preferences.edit();
        isFirstTime = app_preferences.getBoolean("isFirstTime", true);


    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_login) {

            if (edtUserName.getText().toString().length() == 0) {

                edtUserName.setError("Enter Username");
            } else if (edtPassword.getText().toString().length() == 0) {
                edtPassword.setError("Enter Password");
            } else {

                Constant.CURRENT_USER_ID = edtUserName.getText().toString();
                Toast.makeText(LoginScreen.this, Constant.CURRENT_USER_ID + "", Toast.LENGTH_SHORT).show();
                AsyncCallWS ws = new AsyncCallWS();
                ws.execute();




                /*Intent intentLandingPage=new Intent(getApplication(),LandingPage.class);
                startActivity(intentLandingPage);
                finish();*/

            }

        }

    }


    class AsyncCallWS extends AsyncTask<String, Void, Void> {
        final SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(LoginScreen.this);
        ArrayList<SoapObject> sp = new ArrayList<>();
        SoapObject ws;
        final SharedPreferences.Editor editor = app_preferences.edit();

        @Override
        protected Void doInBackground(String... params) {
            //Invoke webservice


            try {
                ws = WebService.invokeHelloWorldWS(sp, "validateUser");
            } catch (Exception e) {
                Toast.makeText(LoginScreen.this, "Bad Internet. Try again.", Toast.LENGTH_SHORT).show();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Set response
            pd_login.dismiss();
            //Log.v("ONMESSAGE", "Dipya" + ws.getProperty("blockCode").toString());

//            Log.d("Ranjeet",result);


            if (ws.getProperty("uid") != null) {
                editor.putString("uid", ws.getProperty("uid").toString());
            } else {
                editor.putString("uid", "Not Available");
                Toast.makeText(LoginScreen.this, "Provide correct User credentials", Toast.LENGTH_LONG).show();
                return;

            }


            if (ws.getProperty("designation") != null) {

                sessionManager.setDesignation(ws.getProperty("designation").toString());
//                editor.putString("designation", ws.getProperty("designation").toString());
            } else {
                editor.putString("designation", "Not Available");
            }


            if (ws.getProperty("userLevel") != null) {
                editor.putString("userLevel", ws.getProperty("userLevel").toString());
            } else {
                editor.putString("userLevel", "Not Available");
            }


            if (ws.getProperty("userCode") != null) {
                //editor.putString("userCode", ws.getProperty("userCode").toString());
                sessionManager.setUsercode(ws.getProperty("userCode").toString());
            } else {
                editor.putString("userCode", "Not Available");
            }


            if (ws.getProperty("curkmsyear") != null)

            {
                String arrVal[] = ws.getProperty("curkmsyear").toString().split(":");
                editor.putString("kms_year", arrVal[0]);
                editor.putString("kms_season", arrVal[1]);

                String arrVal1[] = arrVal[0].split("-");
                editor.putString("kms_year1", arrVal1[0]);
                editor.putString("kms_year2", arrVal1[1]);


                Log.d("Dipya", "DipyaCurkmsyear" + arrVal1[1]);
                Log.d("Dipya", "DipyaCurkmsseason" + arrVal[1]);

                Log.d("Dipya", "Curkmsfull" + arrVal[0]);

                Log.d("Ranjeet", "RanjeetCurkmsyear" + ws.getProperty("curkmsyear").toString());
                sessionManager.setCurkmsyear(ws.getProperty("curkmsyear").toString());

                sessionManager.setCurkmsyearfull(arrVal[0]);

                sessionManager.setCurkmsyear1("20" + arrVal1[1]);
                sessionManager.setCurkmsyear2(arrVal[1]);

            } else {
//                Log.d("Ranjeet","Curtyear"+curkmsyear);

                editor.putString("curkmsyear", "Not Available");
            }


            if (ws.getProperty("blockCode") != null) {
                //Log.v("ONMESSAGE", ws.getProperty("blockCode").toString());
                editor.putString("blockCode", ws.getProperty("blockCode").toString());
            } else {
                editor.putString("blockCode", "Not Available");
            }


            if (ws.getProperty("distCode") != null) {
                editor.putString("distCode", ws.getProperty("distCode").toString());
            } else {
                editor.putString("distCode", "Not Available");
            }


            if (ws.getProperty("pacsCode") != null) {
                editor.putString("pacsCode", ws.getProperty("pacsCode").toString());
            } else {
                editor.putString("pacsCode", "Not Available");
            }


            editor.putBoolean("isFirstTime", false);
            editor.commit();


            sessionManager.setLogin();
            Intent intent = new Intent(LoginScreen.this, LandingPage.class);
            startActivity(intent);

            LoginScreen.this.finish();

        }

        @Override
        protected void onPreExecute() {
            //Make ProgressBar invisible
            SoapObject soUid = new SoapObject("http://webservice", "mynamespace");
            soUid.addProperty("uid", edtUserName.getText().toString());
            soUid.addProperty("pwd", edtPassword.getText().toString());

            // SoapObject soPwd = new SoapObject("pid", mPasswordView.getText().toString());
            sp.add(soUid);

            //sp.add(soPwd);
            Log.v("DIPYA", "" + sp.size());
            pd_login = ProgressDialog.show(LoginScreen.this, null, "Logging In....Please wait", true);
            pd_login.setCancelable(false);


        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.ic_drawer : R.drawable.error);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(dialogIntent);

            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

}
