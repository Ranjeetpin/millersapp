package com.millerapp.millerapp.Models;

import java.io.Serializable;

/**
 * Created by manav boro on 8/26/2016.
 */
public class Information implements Serializable{
    public String V_REG_NO;
    public long UID = 0;
    public String mTRP = "";
    public String mPACS_CODE;
    public String mMY_CODE;
    public String mYEAR;
    public String mSEASON;
    public String mTR_PASS_NO;
    public String mCM_CODE;
    public String mBAG;
    public String mQTY;
    public String mREC_DATETIME;
    public String mIS_DELETED;
    public String status;
    public String QRCode;

    public String getV_REG_NO() {
        return V_REG_NO;
    }

    public long getUID() {
        return UID;
    }

    public String getmTRP() {
        return mTRP;
    }

    public String getmPACS_CODE() {
        return mPACS_CODE;
    }

    public String getmMY_CODE() {
        return mMY_CODE;
    }

    public String getmYEAR() {
        return mYEAR;
    }

    public String getmSEASON() {
        return mSEASON;
    }

    public String getmTR_PASS_NO() {
        return mTR_PASS_NO;
    }

    public String getmCM_CODE() {
        return mCM_CODE;
    }

    public String getmBAG() {
        return mBAG;
    }

    public String getmQTY() {
        return mQTY;
    }

    public String getmREC_DATETIME() {
        return mREC_DATETIME;
    }

    public String getmIS_DELETED() {
        return mIS_DELETED;
    }

    public String getStatus() {
        return status;
    }

    public String getQRCode() {
        return QRCode;
    }
}
