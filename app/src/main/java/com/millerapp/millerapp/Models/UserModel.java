package com.millerapp.millerapp.Models;

/**
 * Created by Ranjeet on 8/31/2016.
 */
public class UserModel {
    public long uid;
    public String userName;
    public String password;

    public long getUid() {
        return uid;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
