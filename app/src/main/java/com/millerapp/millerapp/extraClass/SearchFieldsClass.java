package com.millerapp.millerapp.extraClass;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.millerapp.millerapp.AdpterClass.TransitListAdapter;
import com.millerapp.millerapp.Models.Information;
import com.millerapp.millerapp.R;

import java.util.List;

/**
 * Created by user on 8/23/16.
 */
public class SearchFieldsClass extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        //status bar color change
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.black));
        }
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();

        List<Information> data = (List<Information>) bundle.getSerializable("value");
        Toast.makeText(SearchFieldsClass.this, data.size() + "", Toast.LENGTH_SHORT).show();
        for (Information current : data) {
            Log.d("market yard = ", current.getmPACS_CODE());
            Log.d("society = ", current.getmMY_CODE());

        }
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        TransitListAdapter transitListAdapter = new TransitListAdapter(data);
        Log.d("Ranjeet", transitListAdapter.toString());
        recyclerView.setHasFixedSize(true);

        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(transitListAdapter);
    }


}
