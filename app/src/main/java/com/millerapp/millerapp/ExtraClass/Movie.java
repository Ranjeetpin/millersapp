package com.millerapp.millerapp.extraClass;

/**
 * Created by Lincoln on 15/01/16.
 */
public class Movie {
    public String getSociety() {
        return Society;
    }

    public void setSociety(String society) {
        Society = society;
    }

    public String getTpNo() {
        return TpNo;
    }

    public void setTpNo(String tpNo) {
        TpNo = tpNo;
    }

    public String getTpDate() {
        return TpDate;
    }

    public void setTpDate(String tpDate) {
        TpDate = tpDate;
    }

    public String getPpc() {
        return ppc;
    }

    public void setPpc(String ppc) {
        this.ppc = ppc;
    }

    public String getReceivedOn() {
        return ReceivedOn;
    }

    public void setReceivedOn(String receivedOn) {
        ReceivedOn = receivedOn;
    }

    public String getVechilNumber() {
        return vechilNumber;
    }

    public void setVechilNumber(String vechilNumber) {
        this.vechilNumber = vechilNumber;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    private String TpDate;
    private String TpNo;
    private String Society;
    private String ppc;
    private String ReceivedOn;
    private String vechilNumber;
    private String weight;

    public String getScanCode() {
        return ScanCode;
    }

    public void setScanCode(String scanCode) {
        ScanCode = scanCode;
    }

    private String ScanCode;

    public String getNoOfBags() {
        return noOfBags;
    }

    public void setNoOfBags(String noOfBags) {
        this.noOfBags = noOfBags;
    }

    private String noOfBags;

    public Movie() {
    }

    public Movie(String TpDate, String TpNo, String Society, String ppc, String ReceivedOn, String vechilNumber, String weight, String noOfBags,String ScanCode) {
        this.TpDate = TpDate;
        this.TpNo = TpNo;
        this.Society = Society;
        this.ppc = ppc;
        this.ReceivedOn = ReceivedOn;
        this.vechilNumber = vechilNumber;
        this.weight = weight;
        this.noOfBags = noOfBags;
        this.ScanCode=ScanCode;
    }


}
