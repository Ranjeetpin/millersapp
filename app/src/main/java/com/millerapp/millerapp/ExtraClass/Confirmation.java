package com.millerapp.millerapp.extraClass;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.millerapp.millerapp.R;
import com.millerapp.millerapp.mainClass.AddNewData;

/**
 * Created by Ranjeet on 8/28/2016.
 */
public class Confirmation extends DialogFragment implements View.OnClickListener {
    Button Ok, Cancel,btnNo,btnYes;
  TextView textMessage;
    //    Communicator communicator;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        communicator = (Communicator) activity;
    }
    @Nullable
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.confirmation, null);
        builder.setView(view);
        Dialog dialog = builder.create();
        btnNo= (Button) view.findViewById(R.id.btnYes);
        btnYes= (Button) view.findViewById(R.id.btnNo);
        Ok = (Button) view.findViewById(R.id.ok);

//        Cancel = (Button) view.findViewById(R.id.cancel);
//        editText = (EditText) view.findViewById(R.id.editText);
//        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        setCancelable(true);

        btnNo.setOnClickListener(this);
//        Cancel.setOnClickListener(this);
        return dialog;
    }

    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.btnNo){
            dismiss();
        }
        else if(v.getId()==R.id.btnYes){


            Intent intent=new Intent(getActivity(), AddNewData.class);
            startActivity(intent);

        }



    }
}

