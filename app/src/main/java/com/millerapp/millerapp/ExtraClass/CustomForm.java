package com.millerapp.millerapp.extraClass;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.millerapp.millerapp.R;
import com.millerapp.millerapp.mainClass.AddNewData;
import com.millerapp.millerapp.mainClass.LandingPage;

/**
 * Created by Ranjeet on 8/31/2016.
 */
public class CustomForm  extends DialogFragment implements View.OnClickListener {


    Button Ok, Cancel,scanCode,save,buttonNo,buttonYes;
    View view;

    EditText editText;
    AlertDialog.Builder builder;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.custom_form, null);
        builder.setView(view);
        Dialog dialog = builder.create();
//        Ok = (Button) view.findViewById(R.id.ok);

        buttonNo= (Button) view.findViewById(R.id.buttonNo);
        buttonYes= (Button) view.findViewById(R.id.buttonYes);
//
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        setCancelable(true);
        buttonNo.setOnClickListener(this);
        buttonYes.setOnClickListener(this);
//        Ok.setOnClickListener(this);

        return dialog;
    }



    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.buttonNo){

            dismiss();

//            Intent intentback = new Intent(getActivity().getApplicationContext(), LandingPage.class);
//           intentback.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intentback.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intentback);
//            getActivity().finish();

        }

        else if(v.getId()==R.id.buttonYes) {

            Intent intent= new Intent(getActivity().getApplicationContext(), AddNewData.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();


        }
    }
}
