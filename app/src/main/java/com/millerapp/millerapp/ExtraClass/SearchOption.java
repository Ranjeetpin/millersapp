package com.millerapp.millerapp.extraClass;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.millerapp.millerapp.Database.DatabaseMethods;
import com.millerapp.millerapp.Models.Information;
import com.millerapp.millerapp.R;
import com.millerapp.millerapp.Util.ValuesStatic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.millerapp.millerapp.Util.ValuesStatic.spinnerHashMarketYard;
import static com.millerapp.millerapp.Util.ValuesStatic.spinnerHashSociety;

/**
 * Created by Ranjeet on 8/20/2016.
 */
public class SearchOption extends DialogFragment implements View.OnClickListener {
    Button Ok, Cancel;
    EditText editText;

    Spinner spinnerSociety, SpinerMarketYard;
    ValuesStatic valuesStatic = new ValuesStatic();
    String society, marketYard;

    //    Communicator communicator;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        communicator = (Communicator) activity;
    }

    @Nullable
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.search_layout, null);
        builder.setView(view);
        Dialog dialog = builder.create();

        spinnerSociety = (Spinner) view.findViewById(R.id.spinnerMarketYard);
        SpinerMarketYard = (Spinner) view.findViewById(R.id.spinnerSociety);

        List<String> listSociety = new ArrayList<String>(spinnerHashSociety);
        List<String> listMarketYard = new ArrayList<String>(spinnerHashMarketYard);

        final ArrayAdapter<String> dataAdapterSociety = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listSociety);
        // Drop down layout style - list view with radio button
        dataAdapterSociety.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinnerSociety.setAdapter(dataAdapterSociety);
        spinnerSociety.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                society = dataAdapterSociety.getItem(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        final ArrayAdapter<String> dataAdaptermarketYard = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listMarketYard);
        // Drop down layout style - list view with radio button
        dataAdaptermarketYard.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        SpinerMarketYard.setAdapter(dataAdaptermarketYard);
        SpinerMarketYard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                marketYard = dataAdaptermarketYard.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Ok = (Button) view.findViewById(R.id.ok);

//        Cancel = (Button) view.findViewById(R.id.cancel);
//        editText = (EditText) view.findViewById(R.id.editText);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        setCancelable(true);

        Ok.setOnClickListener(this);
//        Cancel.setOnClickListener(this);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), SearchFieldsClass.class);


        DatabaseMethods methods = new DatabaseMethods(getActivity());
        List<Information> searchedData = methods.getSearchedData(society, marketYard, "", "");
        Bundle bundle = new Bundle();
        bundle.putSerializable("value", (Serializable) searchedData);
        intent.putExtras(bundle);
        startActivity(intent);

        dismiss();

    }
}
