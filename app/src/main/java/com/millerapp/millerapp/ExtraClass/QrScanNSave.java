package com.millerapp.millerapp.extraClass;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.zxing.Result;
import com.millerapp.millerapp.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Ranjeet on 8/20/2016.
 */
public class QrScanNSave extends DialogFragment implements View.OnClickListener,ZXingScannerView.ResultHandler  {
        Button Ok, Cancel,scanCode,save;
        View view;

        EditText editText;
    AlertDialog.Builder builder;
        private ZXingScannerView mScannerView;


        //    Communicator communicator;
@Override
public void onAttach(Activity activity) {
        super.onAttach(activity);
//        communicator = (Communicator) activity;
        }
@Nullable
@Override
public Dialog onCreateDialog(Bundle savedInstanceState) {
         builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
 view = inflater.inflate(R.layout.qr_save_data, null);
        builder.setView(view);
        Dialog dialog = builder.create();
        Ok = (Button) view.findViewById(R.id.ok);

        scanCode= (Button) view.findViewById(R.id.scanCode);
        save= (Button) view.findViewById(R.id.save);
//
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        setCancelable(true);
        save.setOnClickListener(this);
        scanCode.setOnClickListener(this);
        Ok.setOnClickListener(this);

        return dialog;
        }

@Override
public void onClick(View v) {

    if(v.getId()==R.id.scanCode){

        QrScanner();
        Log.d("Ranjeet","Ranjeet Scan Button Clicked");

        builder.show();


    }


        }

    public void QrScanner(){


        mScannerView = new ZXingScannerView(getActivity().getApplicationContext());
        // Programmatically initialize the scanner view


        getDialog().setContentView(mScannerView);
//        builder.setView(mScannerView);
        Log.d("Ranjeet","Ranbjeet clickc checked once");

//          getActivity().getApplicationContext().setContentView(mScannerView);




        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera

    }

        @Override
        public void handleResult(Result result) {

                Toast.makeText(getActivity().getApplication(), result.getText(), Toast.LENGTH_SHORT).show();

                if(result.getBarcodeFormat().toString().equalsIgnoreCase("QR_CODE")){

                       mScannerView.stopCamera();
                        Log.d("Ranjeet","Ranjeet QRCODE SCAN"+result.getText());


                }
                else{

                        Toast.makeText(getActivity().getApplicationContext(), "Not A Valid QR Code", Toast.LENGTH_SHORT).show();

                        mScannerView.resumeCameraPreview(this);

                }

        }

        }
