package com.millerapp.millerapp.AdpterClass;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.millerapp.millerapp.Models.Information;
import com.millerapp.millerapp.Util.ValuesStatic;
import com.millerapp.millerapp.extraClass.Movie;
import com.millerapp.millerapp.R;

import java.util.List;

/**
 * Created by Ranjeet on 8/19/2016.
 */
public class TransitListAdapter extends RecyclerView.Adapter<TransitListAdapter.MyViewHolder> {

    private List<Information> moviesList;
    View itemView;

    ValuesStatic valueStatic = new ValuesStatic();

    public TransitListAdapter(List<Information> movieList) {
        this.moviesList = movieList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TpDate, TpNo, Society, ppc, ReceivedOn, vechilNumber, weight, noOfBags, colorStatus;
        ImageView imageDelete;


        public MyViewHolder(View view) {
            super(view);
            TpDate = (TextView) view.findViewById(R.id.TpDate);
            TpNo = (TextView) view.findViewById(R.id.TpNo);
            Society = (TextView) view.findViewById(R.id.Society);
            ppc = (TextView) view.findViewById(R.id.ppc);
            ReceivedOn = (TextView) view.findViewById(R.id.ReceivedOn);
            vechilNumber = (TextView) view.findViewById(R.id.Vechileno);
            weight = (TextView) view.findViewById(R.id.WeightNumber);
            noOfBags = (TextView) view.findViewById(R.id.noOfBags);
            colorStatus = (TextView) view.findViewById(R.id.colorStatus);
            imageDelete = (ImageView) view.findViewById(R.id.imageDelete);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Information movie = moviesList.get(position);
        holder.TpDate.setText(movie.getmTRP());
        holder.TpNo.setText(movie.getmPACS_CODE());
        holder.Society.setText(movie.getmMY_CODE());
        holder.ppc.setText(movie.getmYEAR());
        holder.ReceivedOn.setText(movie.getmSEASON());
        holder.vechilNumber.setText(movie.getV_REG_NO());
        holder.weight.setText(movie.getmBAG());
        holder.noOfBags.setText(movie.getmQTY());

        valueStatic.spinnerSociety.add(movie.getmMY_CODE());
        valueStatic.spinnerMarketyard.add(movie.getmPACS_CODE());

        valueStatic.spinnerHashSociety.add(movie.getmMY_CODE());
        valueStatic.spinnerHashMarketYard.add(movie.getmPACS_CODE());

//        movie.getS



        if (position == 1 || position == 2 || position == 3 || position == 0) {

            holder.imageDelete.setVisibility(View.INVISIBLE);
            holder.colorStatus.setBackgroundColor(Color.parseColor("#2EFE64"));


        }

        if (position == 4 || position == 5) {

            holder.imageDelete.setVisibility(View.INVISIBLE);
            holder.colorStatus.setBackgroundColor(Color.parseColor("#DF0101"));

        }

        if (position == 6 || position == 7) {

            holder.imageDelete.setVisibility(View.VISIBLE);
//            holder.colorStatus.setBackgroundColor(Color.parseColor("#00FFFF"));

        }


        if (position % 2 == 0) {
            holder.itemView.setBackgroundColor(Color.parseColor("#ffffff"));
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#F2F2F2"));
        }

    }


    @Override
    public int getItemCount() {
        return moviesList.size();
    }


}
