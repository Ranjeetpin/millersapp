package com.millerapp.millerapp.mainClass;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Camera;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.millerapp.millerapp.Database.DatabaseMethods;
import com.millerapp.millerapp.R;
import com.millerapp.millerapp.Util.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class AddNewData extends AppCompatActivity implements View.OnClickListener, ZXingScannerView.ResultHandler {

    Button btn_scanCode, btn_save;
    private ZXingScannerView mScannerView = null;
    ArrayList<String> arrayToken;



    String ResultValue = null;
    TextView txtscancode, TpDat, ReceviedOn;
    String vechilenumber, Society, marketYard, TpDate, TpNumber, NoOfBags, QuantityInQtl, scanCodeValid;
    EditText vechileNo, tpNo, noOfBags, QuantityinQtl, MarketYard, society;
    //    Spinner society;
    SessionManager sessionManger;

    ImageView backButton;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Time now;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        sessionManger = new SessionManager(getApplicationContext());


        now = new Time();
        now.setToNow();

//        backButton = (ImageView) findViewById(R.id.backButton);
        ReceviedOn = (TextView) findViewById(R.id.ReceviedOn);
        vechileNo = (EditText) findViewById(R.id.vechileNo);
        tpNo = (EditText) findViewById(R.id.tpNo);
        noOfBags = (EditText) findViewById(R.id.noOfBags);
        QuantityinQtl = (EditText) findViewById(R.id.QuantityinQtl);
        txtscancode = (TextView) findViewById(R.id.txtscancode);
        MarketYard = (EditText) findViewById(R.id.MarketYard);
        society = (EditText) findViewById(R.id.society);
        TpDat = (TextView) findViewById(R.id.TpDate);
        ReceviedOn.setText(calender() + " " + now.format("%k:%M"));

//        backButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });

        try {
            Intent intentGetValue = getIntent();

//            ReceviedOn.setText();
            vechileNo.setText(intentGetValue.getStringExtra("vechilNumber"));
            tpNo.setText(intentGetValue.getStringExtra("TpNumber"));
            noOfBags.setText(intentGetValue.getStringExtra("NoOfBags"));
            QuantityinQtl.setText(intentGetValue.getStringExtra("QuantityInQtl"));
            txtscancode.setText(intentGetValue.getStringExtra("scanCodeValid"));
            society.setText(intentGetValue.getStringExtra("Society"));
            MarketYard.setText(intentGetValue.getStringExtra("marketYard"));
            TpDat.setText(intentGetValue.getStringExtra("TpDate"));


        } catch (Exception e) {

            Log.d("Ranjeet", "Ranjeet Value logoutIntent " + e);
        }

        btn_scanCode = (Button) findViewById(R.id.btn_scanCode);
        btn_save = (Button) findViewById(R.id.btn_save);

        arrayToken = new ArrayList<String>();

        btn_scanCode.setOnClickListener(this);

        TpDat.setOnClickListener(this);
        btn_save.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_scanCode) {

            Intent intentScanCode = new Intent(getApplicationContext(), BarCodeScan.class);
            intentScanCode.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intentScanCode.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intentScanCode);
            finish();


//            QrScanner();
        } else if (v.getId() == R.id.btn_save) {
String scanCodeVAlue="";


                if(txtscancode.getText().toString().length()==0){

                    scanCodeVAlue="notpresent";
                }
                else{
                    scanCodeVAlue=txtscancode.getText().toString();
                }
                Log.d("Ranjeet","scanCode"+txtscancode.getText().toString());


            DatabaseMethods methods = new DatabaseMethods(this);
            long id = methods.insertItem(society.getText().toString(), MarketYard.getText().toString(),
                    sessionManger.getCurkmsyearfull(), sessionManger.getCurkmsyear2(), tpNo.getText().toString(), sessionManger.getUsercode(), noOfBags.getText().toString(),
                    QuantityinQtl.getText().toString(), vechileNo.getText().toString(), TpDat.getText().toString(), "not deleted", "not uploaded", scanCodeVAlue,sessionManger.getUsercode());
//            methods.insertItem(calender() + " " + now.format("%k:%M"), "123456", "abcdef", "2016-17", "rabi", "mc 001", "mc 001", "16", "100", "27-aug-2016", "29-aug-1016", "no", "0", "987654321");

            if (id != -1) {
                finish();
            }

        } else if (v.getId() == R.id.TpDate) {
//            TpDat.setText(calender());

            final Calendar calender = Calendar.getInstance();
            mYear = calender.get(Calendar.YEAR);
            mMonth = calender.get(Calendar.MONTH);
            mDay = calender.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox
                            TpDat.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
//            dpd.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
            dpd.show();

        }


    }


    public void QrScanner() {


        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera

    }

    @Override
    public void handleResult(Result result) {

        if (result.getBarcodeFormat().toString().equalsIgnoreCase("QR_CODE")) {
//            mScannerView.stopCamera();

            String scanMessage = result.getText();
            StringTokenizer stringToken = new StringTokenizer(scanMessage, "^");

            while (stringToken.hasMoreTokens()) {
                String tokenValue = stringToken.nextToken();
                Log.d("Ranjeet", "stringToken" + tokenValue);
                arrayToken.add(tokenValue);
            }

            Log.d("Ranjeet", "ArrayValue" + arrayToken.get(0));


            vechilenumber = arrayToken.get(0);
            String societycode = arrayToken.get(1);
            String societyName = arrayToken.get(2);
            Society = societyName + "(" + societycode + ")";

            String marketCode = arrayToken.get(3);
            String marketName = arrayToken.get(4);

            marketYard = marketCode + "(" + marketName + ")";

            TpDate = arrayToken.get(5);
            TpNumber = arrayToken.get(6);
            NoOfBags = arrayToken.get(7);
            QuantityInQtl = arrayToken.get(8);
            scanCodeValid = arrayToken.get(9);


            Toast.makeText(AddNewData.this, "Want to stop", Toast.LENGTH_SHORT).show();
            mScannerView.stopCamera();
//            mScannerView.stopCameraPreview();


            mScannerView.stopCameraPreview();


            Log.d("Ranjeet", "ScanValue" + result.getText());
////            return startActivity();
            ResultValue = result.toString();

            Intent intent = new Intent(getApplicationContext(), AddNewData.class);
            intent.putExtra("vechilNumber", vechilenumber);
            intent.putExtra("Society", Society);
            intent.putExtra("TpDate", TpDate);
            intent.putExtra("TpNumber", TpNumber);
            intent.putExtra("NoOfBags", NoOfBags);
            intent.putExtra("QuantityInQtl", QuantityInQtl);
            intent.putExtra("scanCodeValid", scanCodeValid);
            intent.putExtra("marketYard", marketYard);

            startActivity(intent);
            ResultValue = null;
            finish();

        } else {

            Toast.makeText(AddNewData.this, "Not A Valid QR Code", Toast.LENGTH_SHORT).show();
//
            mScannerView.resumeCameraPreview(this);

        }

        mScannerView.destroyDrawingCache();

    }

    @Override
    public void onBackPressed() {
        Toast.makeText(AddNewData.this, "onBack press", Toast.LENGTH_SHORT).show();


        try {
            mScannerView.startCamera();
            mScannerView.stopCamera();

        } catch (Exception e) {
        }
        if (ResultValue == null) {
            Intent intentHome = new Intent(getApplicationContext(), LandingPage.class);
            startActivity(intentHome);
            finish();

        } else {
            Intent intent = new Intent(getApplicationContext(), AddNewData.class);
            startActivity(intent);
//            ResultValue=null;
            finish();


        }
        super.onBackPressed();


    }


    public String calender() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;

    }


    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.ic_drawer : R.drawable.error);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(dialogIntent);

            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

}
