package com.millerapp.millerapp.mainClass;


import me.dm7.barcodescanner.zxing.ZXingScannerView;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.app.ProgressDialog;

import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;

import com.google.zxing.Result;
import com.millerapp.millerapp.AdpterClass.TransitListAdapter;
import com.millerapp.millerapp.Constant;
import com.millerapp.millerapp.Database.DBHelper;
import com.millerapp.millerapp.Database.DatabaseMethods;
import com.millerapp.millerapp.Models.Information;
import com.millerapp.millerapp.Webservice.WebService;
import com.millerapp.millerapp.Webservice.WebServiceEnvelop;
import com.millerapp.millerapp.extraClass.Movie;

import com.millerapp.millerapp.extraClass.SearchOption;
import com.millerapp.millerapp.R;
import com.millerapp.millerapp.Util.SessionManager;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class LandingPage extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;

    private RecyclerView recyclerView;
    SharedPreferences app_preferences;
    SessionManager sessionMnager;
    SoapPrimitive result;
    List<Information> List = new ArrayList<>();
    SoapObject resultrrrr;

    TransitListAdapter transitListAdapter;
    FloatingActionButton fab;
    Button Search, Add;
    TextView search, saveData, dataSync;
    TextView txt_kmsYear, txt_desgn, txt_desgn_appbar;
    boolean sysn = true;
    String vechilenumber, Society, marketYard, TpDate, TpNumber, NoOfBags, QuantityInQtl, scanCodeValid;
    Boolean hierarchy = false;
    Boolean showalert = true;
    AlertDialog alertDialog;
    ImageView imgReload;
    StringBuffer resultRanj;
    LinearLayout linearButtonSearch;
    ArrayList<String> arrayToken;

    SessionManager sessionManager;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
//        getSupportActionBar().hide();


        IntentFilter filter = new IntentFilter("broadCastName");


        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle results = getResultExtras(true);
                hierarchy = intent.getExtras().getBoolean("message");
//
                if (hierarchy) {

//                    submissionData();
//                    Toast.makeText(HistoryDetails.this, "Internet Connect ", Toast.LENGTH_LONG).show();
                    Log.d("Ranjeet", "Intenet back");
                    try {
                        alertDialog.dismiss();
//
                        showalert = true;
                    } catch (Exception e) {
                        Log.d("Ranjeet", hierarchy + "RAnjeet receive valuae of receiver");
                    }


                } else {
                    if (showalert) {
                        showAlertDialog(LandingPage.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                        showalert = false;

                        Log.d("Ranjeet", hierarchy + "RAnjeet receive valuae of receiver");
                    }
                }


                Log.d("Ranjeet", hierarchy + "RAnjeet receive valuae of receiver");
            }
        }, filter);


        arrayToken = new ArrayList<String>();

        DBHelper helper = new DBHelper(this);
        SQLiteDatabase database = helper.getWritableDatabase();


        sessionMnager = new SessionManager(getApplicationContext());

        DatabaseMethods methods = new DatabaseMethods(this);
        List = methods.getAllData();

        if (List.size() == 0) {
//            callWebserView();

            AsyncCallgetTransistPass ranjeetcheck = new AsyncCallgetTransistPass();
            ranjeetcheck.execute();

<<<<<<< HEAD
        AsyncCallgetTransistPass check= new AsyncCallgetTransistPass();
        check.execute();
=======
        }
>>>>>>> 75e42d1f8a0d2c92e350dfe0c59e81f9fa170695

        imgReload = (ImageView) findViewById(R.id.imgReload);
        imgReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ReloadClass reload = new ReloadClass();
                reload.execute();
            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        search = (TextView) findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (List.size() > 0) {
                    FragmentManager manager = getSupportFragmentManager();
                    SearchOption myDialog = new SearchOption();
                    myDialog.show(manager, "MyDialog");

                } else {

                    Toast.makeText(LandingPage.this, "No Value To Search", Toast.LENGTH_SHORT).show();

                }
            }
        });


        saveData = (TextView) findViewById(R.id.saveData);
        saveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentScan = new Intent(getApplicationContext(), BarCodeScan.class);
//                intentScan.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentScan);
//                finish();

            }
        });

        dataSync = (TextView) findViewById(R.id.dataSync);
        dataSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sysn) {
                    sysn = false;
                    dataSync.setText("SYNC OFF");
//                    dataSync.setBackgroundColor(Color.parseColor("#ccedd7"));
                } else {
                    sysn = true;
                    dataSync.setText("SYNC ON");
//                    dataSync.setBackgroundColor(Color.parseColor("#08AE9E"));
                }
            }
        });

        txt_kmsYear = (TextView) findViewById(R.id.txt_year);
        txt_desgn = (TextView) findViewById(R.id.txt_designation);
        txt_desgn_appbar = (TextView) findViewById(R.id.textView_desgn);

        txt_kmsYear.setText("KMS " + sessionMnager.getCurkmsyear());
        txt_desgn.setText(sessionMnager.getDesignation());
        txt_desgn_appbar.setText(sessionMnager.getDesignation());

        transitListAdapter = new TransitListAdapter(List);
        Log.d("Ranjeet", transitListAdapter.toString());
        recyclerView.setHasFixedSize(true);

        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(transitListAdapter);


    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        DatabaseMethods methods = new DatabaseMethods(this);
        List<Information> users = methods.isUserAvailable(Constant.CURRENT_USER_ID);
        if (users.size() == 0) {
            //callWebService();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        DatabaseMethods methods = new DatabaseMethods(this);
        List = methods.getAllData();

        transitListAdapter = new TransitListAdapter(List);
        Log.d("Ranjeet", transitListAdapter.toString());
        recyclerView.setHasFixedSize(true);

        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(transitListAdapter);
    }

    @Override
    public void handleResult(Result result) {

        if (result.getBarcodeFormat().toString().equalsIgnoreCase("QR_CODE")) {
//            mScannerView.stopCamera();

            String scanMessage = result.getText();
            StringTokenizer stringToken = new StringTokenizer(scanMessage, "^");

            while (stringToken.hasMoreTokens()) {
                String tokenValue = stringToken.nextToken();
                Log.d("Ranjeet", "stringToken" + tokenValue);
                arrayToken.add(tokenValue);
            }

            Log.d("Ranjeet", "ArrayValue" + arrayToken.get(0));


            vechilenumber = arrayToken.get(0);
            String societycode = arrayToken.get(1);
            String societyName = arrayToken.get(2);
            Society = societyName + "(" + societycode + ")";

            String marketCode = arrayToken.get(3);
            String marketName = arrayToken.get(4);

            marketYard = marketCode + "(" + marketName + ")";

            TpDate = arrayToken.get(5);
            TpNumber = arrayToken.get(6);
            NoOfBags = arrayToken.get(7);
            QuantityInQtl = arrayToken.get(8);
            scanCodeValid = arrayToken.get(9);


            Toast.makeText(LandingPage.this, "Want to stop", Toast.LENGTH_SHORT).show();
            mScannerView.stopCamera();
//            mScannerView.stopCameraPreview();


            mScannerView.stopCameraPreview();


            Log.d("Ranjeet", "ScanValue" + result.getText());
////            return startActivity();
//            ResultValue = result.toString();

            Intent intent = new Intent(getApplicationContext(), AddNewData.class);
            intent.putExtra("vechilNumber", vechilenumber);
            intent.putExtra("Society", Society);
            intent.putExtra("TpDate", TpDate);
            intent.putExtra("TpNumber", TpNumber);
            intent.putExtra("NoOfBags", NoOfBags);
            intent.putExtra("QuantityInQtl", QuantityInQtl);
            intent.putExtra("scanCodeValid", scanCodeValid);
            intent.putExtra("marketYard", marketYard);

            startActivity(intent);
//            ResultValue = null;
            finish();

        } else {

            Toast.makeText(LandingPage.this, "Not A Valid QR Code", Toast.LENGTH_SHORT).show();

//            mScannerView.resumeCameraPreview(this);


        }


    }


<<<<<<< HEAD
    class AsyncCallgetTransistPass extends AsyncTask<String, String, String> {
=======




    class AsyncCallgetTransistPass extends AsyncTask<String, Void, Void> {
>>>>>>> ed5e3296fd5176efabd88c7b065aa3c16f78c80c
        //final SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(SecondActivity.this);
        ArrayList<SoapObject> sp = new ArrayList<>();
        SoapSerializationEnvelope ws = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);


        private String NAMESPACE = "http://webservice";

        private String URL = "http://ppasodisha.org.in/TmpDshService/services/PurchaseReport?wsdl";
        private String SOAP_ACTION = "http://ppasodisha.org.in/TmpDshService/services/PurchaseReport/";

        //final SharedPreferences.Editor editor = app_preferences.edit();
        @Override
        protected String doInBackground(String... params) {
            //Invoke webservice


//            ws = WebServiceEnvelop.invokeHelloWorldWS(sp, "getTransitPass");


            try {
                SoapObject request = new SoapObject(NAMESPACE, "getTransitPass");

// add paramaters and values

//                request.addProperty("mYEAR", Integer.parseInt(sessionMnager.getCurkmsyear1()));
//                request.addProperty("mSEASON", sessionMnager.getCurkmsyear2());
//                request.addProperty("mCM_CODE", sessionMnager.getUsercode());

                request.addProperty("mYEAR",2015);
                request.addProperty("mSEASON","R");
                request.addProperty("mCM_CODE","M030224");
//
//


                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

//Web method call
                HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                androidHttpTransport.call(SOAP_ACTION + "getTransitPass", envelope);
//get the response
                resultrrrr = (SoapObject) envelope.getResponse();

                resultRanj = new StringBuffer(resultrrrr.toString());

                Log.d("Ranjeet", "Check WEbservice" + resultRanj);

//the response object can be retrieved by its name: result.getProperty("objectName")
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Ranjeet", "SoapError" + e.toString());
            }


            return resultRanj.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            //Set response
            Log.v("DIPYA", "Response"+result);

//
//            ArrayList<String> scripts = new ArrayList<String>();



//                try {
//                SoapObject response = (SoapObject) ws.bodyIn;
//
//                try {
//
//                    Log.v("ONMESSAGE", "Response:- " + result);
//
////
////                    for (int i = 0; i < result.; i++) {
////                        Log.v("ONMESSAGE", response.getProperty(i).toString());
////                        scripts.add(response.getProperty(i).toString());
////
////                    }
////                    //
//
//                } catch (Exception soapFault) {
//                    soapFault.printStackTrace();
//                    Log.v("ONMESSAGE", soapFault.toString());
//                }
//
//
//                //Then call async task for progress data code AsyncCallWSDistrict

//            } catch (Exception e) {
//
//            }
        }

        @Override
        protected void onPreExecute() {


        }

//        @Override
//        protected void onProgressUpdate(Void... values) {
//        }
    }


<<<<<<< HEAD
=======







    class AsyncCalluploadMillerTransitPass extends AsyncTask<String, Void, Void> {
        //final SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(SecondActivity.this);
        ArrayList<SoapObject> sp1 = new ArrayList<>();
        SoapObject ws1;


        //final SharedPreferences.Editor editor = app_preferences.edit();
        @Override
        protected Void doInBackground(String... params) {
            //Invoke webservice


            ws1 = WebService.invokeHelloWorldWS(sp1, "uploadMillerTransitPass");

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Set response
            Log.v("DIPYA", "Response");

        }

        @Override
        protected void onPreExecute() {
            //Make ProgressBar invisible
            SoapObject soUid = new SoapObject("http://webservice", "mynamespace");


            // SoapObject soPwd = new SoapObject("pid", mPasswordView.getText().toString());
            soUid.addProperty("mTRP_DATE", "01/08/2016");
            soUid.addProperty("mPACS_CODE", "S1030201");
            soUid.addProperty("mMY_CODE", "MY030201");
            soUid.addProperty("mYEAR", "2016");
            soUid.addProperty("mSEASON", "RABI");
            soUid.addProperty("mTR_PASS_NO", "34");
            soUid.addProperty("mCM_CODE", "M030224");
            soUid.addProperty("mBAG", "1250");
            soUid.addProperty("mQTY", "624.5");
            soUid.addProperty("V_REG_NO", "OR03BA2334");
            soUid.addProperty("mREC_DATETIME", "01/08/2016");
            soUid.addProperty("mIS_DELETED", "not deleted");


            sp1.add(soUid);

            //sp.add(soPwd);
            Log.v("DIPYA", "" + sp1.size());
           /* pd = ProgressDialog.show(SecondActivity.this, null, "Loading....Please wait", true);
            pd.setCancelable(false);*/


        }
>>>>>>> ed5e3296fd5176efabd88c7b065aa3c16f78c80c



    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.ic_drawer : R.drawable.error);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(dialogIntent);

            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    //webservice 2

    public class ReloadClass extends AsyncTask<Void, Void, Void> {

        private String NAMESPACE = "http://webservice";

        private String URL = "http://ppasodisha.org.in/TmpDshService/services/PurchaseReport?wsdl";
        private String SOAP_ACTION = "http://ppasodisha.org.in/TmpDshService/services/PurchaseReport/";

        @Override
        protected Void doInBackground(Void... params) {


            try {
                SoapObject request = new SoapObject(NAMESPACE, "uploadMillerTransitPass");

// add paramaters and values
                request.addProperty("mTRP_DATE", "30-08-2016");
                request.addProperty("mTR_PASS_NO","143");
                request.addProperty("mPACS_CODE", "S1030201");
                request.addProperty("mMY_CODE", "S1030201");
                request.addProperty("mYEAR",2015);
                request.addProperty("mSEASON", "R");
                request.addProperty("mCM_CODE", "M030224");
                request.addProperty("mBAG", 450);
                request.addProperty("mQTY", "180");
                request.addProperty("mV_REG_NO", "ODTE0322ST");
                request.addProperty("mREC_DATETIME", "30-08-2015");
                request.addProperty("mIS_DELETED", "Y");

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

//Web method call
                HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                androidHttpTransport.call(SOAP_ACTION + "uploadMillerTransitPass", envelope);
//get the response
                result = (SoapPrimitive) envelope.getResponse();

                Log.d("Ranjeet", "Check WEbservice" + result);

//the response object can be retrieved by its name: result.getProperty("objectName")
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Ranjeet", "SoapError" + e.toString());
            }
            return null;
        }


    }


}
