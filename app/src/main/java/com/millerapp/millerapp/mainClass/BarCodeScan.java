package com.millerapp.millerapp.mainClass;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;
import com.millerapp.millerapp.R;
import com.millerapp.millerapp.Util.SessionManager;
import com.millerapp.millerapp.extraClass.CustomForm;
import com.millerapp.millerapp.extraClass.SearchOption;

import java.util.ArrayList;
import java.util.StringTokenizer;

import me.dm7.barcodescanner.core.BarcodeScannerView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class BarCodeScan extends AppCompatActivity implements ZXingScannerView.ResultHandler{


    SessionManager sessionManger;
    private ZXingScannerView mScannerView = null;
    ArrayList<String> arrayToken;
    String vechilenumber, Society, marketYard, TpDate, TpNumber, NoOfBags, QuantityInQtl, scanCodeValid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_code_scan);

        arrayToken = new ArrayList<String>();
        sessionManger= new SessionManager(getApplicationContext());
        QrScanner();

    }


    public void QrScanner() {


        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera

    }
    @Override
    public void handleResult(Result result) {

        if (result.getBarcodeFormat().toString().equalsIgnoreCase("QR_CODE")) {
//            mScannerView.stopCamera();

            String scanMessage = result.getText();
            StringTokenizer stringToken = new StringTokenizer(scanMessage, "^");

            while (stringToken.hasMoreTokens()) {
                String tokenValue = stringToken.nextToken();
                Log.d("Ranjeet", "stringToken" + tokenValue);
                arrayToken.add(tokenValue);
            }

            Log.d("Ranjeet", "ArrayValue" + arrayToken.get(0));

            if(arrayToken.get(0).equalsIgnoreCase("PPAS-TRP")) {

//                Toast.makeText(BarCodeScan.this, "PPAS_TRP SUCCESFULLY", Toast.LENGTH_SHORT).show();

                if(arrayToken.get(1).equals(sessionManger.getUsercode())) {

                    Toast.makeText(BarCodeScan.this, "usercode SUCCESFULLY", Toast.LENGTH_SHORT).show();


                    vechilenumber = arrayToken.get(2);
                    String societycode = arrayToken.get(3);
                    String societyName = arrayToken.get(4);
                    Society = societyName + "(" + societycode + ")";

                    String marketCode = arrayToken.get(5);
                    String marketName = arrayToken.get(6);

                    marketYard = marketCode + "(" + marketName + ")";

                    TpDate = arrayToken.get(7);
                    TpNumber = arrayToken.get(8);
                    NoOfBags = arrayToken.get(9);
                    QuantityInQtl = arrayToken.get(10);
                    scanCodeValid = arrayToken.get(11);


                    Toast.makeText(BarCodeScan.this, "Want to stop", Toast.LENGTH_SHORT).show();
                    mScannerView.stopCamera();
//            mScannerView.stopCameraPreview();


                    mScannerView.stopCameraPreview();


                    Log.d("Ranjeet", "ScanValue" + result.getText());
////            return startActivity();
//            ResultValue = result.toString();

                    Intent intent = new Intent(getApplicationContext(), AddNewData.class);
                    intent.putExtra("vechilNumber", vechilenumber);
                    intent.putExtra("Society", Society);
                    intent.putExtra("TpDate", TpDate);
                    intent.putExtra("TpNumber", TpNumber);
                    intent.putExtra("NoOfBags", NoOfBags);
                    intent.putExtra("QuantityInQtl", QuantityInQtl);
                    intent.putExtra("scanCodeValid", scanCodeValid);
                    intent.putExtra("marketYard", marketYard);

                    startActivity(intent);
//            ResultValue = null;
                    finish();
                }
//                mScannerView.stopCamera();
//                mScannerView.resumeCameraPreview(this);
                else {

                    mScannerView.stopCameraPreview();
                    FragmentManager manager = getSupportFragmentManager();
                    CustomForm myDialog = new CustomForm();
                    myDialog.show(manager, "MyDialog");
                }


            }
//            mScannerView.stopCamera();
//
//            mScannerView.resumeCameraPreview(this);
            else {

                mScannerView.stopCameraPreview();
                FragmentManager manager = getSupportFragmentManager();
                CustomForm myDialog = new CustomForm();
                myDialog.show(manager, "MyDialog");
            }

        } else {

            Toast.makeText(BarCodeScan.this, "Not A Valid QR Code", Toast.LENGTH_SHORT).show();
//
            mScannerView.resumeCameraPreview(this);

        }

        mScannerView.destroyDrawingCache();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
 mScannerView.stopCamera();

    }

    @Override
    protected void onResume() {
        super.onResume();

//        onBackPressed();
//        mScannerView.startCamera();
//mScannerView.resumeCameraPreview(this);
//mScannerView.startCamera();

    }
//
//    @Override
//    protected void onPause() {
//mScannerView.stopCameraPreview();
////        mScannerView.stopCamera();
//        super.onPause();
//    }
}
