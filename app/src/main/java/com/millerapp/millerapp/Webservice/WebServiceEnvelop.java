package com.millerapp.millerapp.Webservice;

/**
 * Created by user on 7/2/16.
 */
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

public class WebServiceEnvelop {
    //Namespace of the Webservice - can be found in WSDL
    private static String NAMESPACE = "http://webservice";
    //Webservice URL - WSDL File location
    private static String URL = "http://ppasodisha.org.in/TmpDshService/services/PurchaseReport?wsdl";
    //SOAP Action URI again Namespace + Web method name
    private static String SOAP_ACTION = "http://ppasodisha.org.in/TmpDshService/services/PurchaseReport/";

    public static SoapSerializationEnvelope invokeHelloWorldWS(ArrayList<SoapObject> sp, String webMethName) {
        String resTxt = null;
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters
        //Log.v("DIPYA", sp.get(0).getProperty("uid") + " " + sp.get(0).getProperty("pwd"));

        Log.v("DIPYA", "1333");
        for(int i = 0; i < sp.get(0).getPropertyCount();i ++) {
            PropertyInfo pi = new PropertyInfo();
            PropertyInfo sayHelloPI = new PropertyInfo();
            sp.get(0).getPropertyInfo(i, pi);
            sayHelloPI.setName(pi.name);
            sayHelloPI.setValue(sp.get(0).getProperty(i).toString());
            Log.v("DIPYA", "Data came " + pi.name + " " + sp.get(0).getProperty(i).toString());
            sayHelloPI.setType(String.class);
            // Add the property to request object
            request.addProperty(sayHelloPI);

        }

        Log.v("DIPYA", "2");


       /* Log.d("DIPYA", pi.name + " : " + sp.get(0).getProperty(0).toString());

        // Set Name

        // Set Value

        sayHelloPI.setName("pwd");
        // Set Value
        sayHelloPI.setValue("ioo");*/
        // Set dataType

        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        SoapPrimitive response ;
        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION+webMethName, envelope);
            // Get the response
            response = (SoapPrimitive) envelope.bodyIn;
            // Assign it to resTxt variable static variable
            resTxt = response.toString();
            Log.v("DIPYA","3" + resTxt);

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
            Log.v("DIPYA", "Error" + e.toString());
            return  null;
        }
        //Return resTxt to calling object
        return envelope;
    }
}
